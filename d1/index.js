const http = require('http');

let directory =[
	{name:"Brandon",
	email:"brandon@gmail.com"},

	{name:"Jobert",
	email:"jobert@gmail.com"}
]

const server = http.createServer((req,res)=>{
	//Get all users
	if(req.url === "/users" && req.method === "GET"){
		res.writeHead(200,{'Content-type':'text/plain'})
		res.end()
	}

	//add new users
	if(req.url === "/users" && req.method === "POST"){
		
		let requestBody = "";

		//the data takes in any data coming in the function and use it to the function
		req.on('data',(data)=>{
			requestBody += data;
		})

		req.on('end', ()=>{
			//requestBody is still in string, with this we need to change it to JSON
			console.log(typeof requestBody)
			//This is how to change the string to JSON
			requestBody = JSON.parse(requestBody)

			//Create a new object representing the new mock db records
			let newUser ={
				"name": requestBody.name,
				"email": requestBody.email
			}
			directory.push(newUser)
			console.log(directory)
		console.log(directory)
		res.writeHead(200,{'Content-Type':"application/json"})
		res.write(JSON.stringify(newUser))
		})

		res.end()
	}
});

server.listen(3000,"localhost",()=>{
	console.log('listening to 3000')
})

// 